import React, { Component } from "react";
import {Btn} from "./styled";

export class Button extends Component {
	constructor() {
		super();

		this.handleClick = this.handleClick.bind(this);
	}

	handleClick() {
		this.props.onClick();
	};

	render() {
		const {text, isClickable = true} = this.props;

		return (
			<React.Fragment>
				<Btn
					text={text}
					isClickable={isClickable}
					{...(isClickable && {onClick:() => this.handleClick()})}
				>
					{text}
				</Btn>
			</React.Fragment>
		);
	}
}
