import React, { Component } from "react";
import {PanelUl} from "./styled";
import {CONST} from "./constants";
import {Button} from "./Button";

export class InfoPanel extends Component {
	constructor() {
		super();
		this.state = {
		};

		this.myLi = this.myLi.bind(this);
		this.showInfo = this.showInfo.bind(this);
	}

	myLi(text, value, counter, clickEvent) {
		if (clickEvent) {
			const updatedText = text + ":" + value;
			return (
				<li key={counter}>
					<Button
						text={updatedText}
						isClickable={true}
						onClick={() => this.props.actionButton(text)}
					/>
				</li>
			);
		} else {
			return <li key={counter}><b>{text}</b>: {value}</li>;
		}
	};

	showInfo() {
		let infoList = [];
		let counter = 0;

		for (let key in this.props) {
			if (this.props[key][2] === CONST.SHOW_IN_INFO_PANEL_LOOP) {
				infoList.push(
					this.myLi(this.props[key][0], this.props[key][1], counter, this.props[key][3])
				);
				counter++;
			}
		}

		return infoList;
	};

	render() {
		const {type} = this.props;

		return (
			<React.Fragment>
				<PanelUl
					type={type}
				>
					{this.showInfo()}
				</PanelUl>
			</React.Fragment>
		);
	}
}
