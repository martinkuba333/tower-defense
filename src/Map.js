import React, { Component } from "react";
import {CONST} from "./constants";
import {Board} from "./styled";
import {Line} from "./Line";
import {Place} from "./Place";

export class Map extends Component {
	constructor() {
		super();
		this.state = {
		};
		
		this.createSquares = this.createSquares.bind(this);
	}

	createSquares() {
		const {gold, updateGold, updateTowersInfo} = this.props;
		let places = [];
		let maxLine = CONST.BOARD_HEIGHT / CONST.SQUARE_SIZE;
		let maxColumn = CONST.BOARD_WIDTH / CONST.SQUARE_SIZE;
		let counter = 0;

		for (let i = 0; i < maxLine; i++) {
			for (let j = 0; j < maxColumn; j++) {
				places.push(
					<Place
						key={counter}
						index={counter}
						line={i}
						column={j}
						maxLine={maxLine - 1}
						maxColumn={maxColumn - 1}
						gold={gold}
						updateGold={updateGold}
						updateTowersInfo={updateTowersInfo}
					/>
				);

				counter++;
			}
		}

		return places;
	};

	render() {
		const {gameStarted} = this.props;

		return (
			<React.Fragment>
				<Board
					gameStarted={gameStarted}
				>
					<Line check={CONST.CHECK_START} />
					<Line check={CONST.CHECK_FINISH} />
					{this.createSquares()}
				</Board>
			</React.Fragment>
		);
	}
}
