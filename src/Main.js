import React, { Component } from "react";
import {CONST} from "./constants";
import {Map} from "./Map";
import {Button} from "./Button";
import {Life} from "./Life";
import {InfoPanel} from "./InfoPanel";
import {
	calculatePosition,
	calculateRotation,
	calculateTowerPosition,
	createEnemies,
	createShots,
	getEnemyBorders,
	getRandom
} from "./function";

let enemyInterval;
let pauseInterval;
let startEnemyInterval = true;

class Main extends Component {
	constructor() {
		super();
		this.state = {
			gameStarted: false,
			level: CONST.GAME_START_LVL,
			gold: CONST.STARTING_GOLD,
			playerLife: CONST.PLAYER_STARTING_LIFE,
			pauseTimer:0,
			enemies: [],
			towers: [],
			shots: [],
		};

		this.handleStartClick = this.handleStartClick.bind(this);
		this.fillDataForEnemies = this.fillDataForEnemies.bind(this);
		this.pause = this.pause.bind(this);
		this.endPause = this.endPause.bind(this);
		this.handleEnemies = this.handleEnemies.bind(this);
		this.gameRunning = this.gameRunning.bind(this);
		this.updateEnemies = this.updateEnemies.bind(this);
		this.newLvl = this.newLvl.bind(this);
		this.removeEnemy = this.removeEnemy.bind(this);
		this.decreasePlayerLife = this.decreasePlayerLife.bind(this);
		this.gameOver = this.gameOver.bind(this);
		this.gameRunning = this.gameRunning.bind(this);
		this.updateGold = this.updateGold.bind(this);
		this.shootShots = this.shootShots.bind(this);
		this.shootShot = this.shootShot.bind(this);
		this.removeShot = this.removeShot.bind(this);
		this.findTarget = this.findTarget.bind(this);
		this.enemyIsHit = this.enemyIsHit.bind(this);
		this.afterHit = this.afterHit.bind(this);
		this.updateTowersInfo = this.updateTowersInfo.bind(this);
	}

	handleStartClick() {
		this.setState({
			gameStarted: true,
		}, this.fillDataForEnemies());

		setInterval(() => {
			this.shootShots(); //remove
		}, 1000);
	};

	fillDataForEnemies() {
		const {level} = this.state;
		let enemiesData = [];

		for (let i = 0; i < CONST.ENEMY_COUNT_LVL[level]; i++) {
			enemiesData.push({
				key: i,
				index: i,
				top: getRandom(0, CONST.SQUARE_SIZE * CONST.PATH_VOLUME - CONST.ENEMY_SIZE),
				left: -i * CONST.ENEMY_START_INDENT,
				location: CONST.ENEMY_LOCATION_FIRST,
				borders: getEnemyBorders(),
				alive: true,
				gold: CONST.ENEMY_GOLD_VALUE,
				hit: 0,
			});
		}

		this.pause(enemiesData);
	};

	pause(enemiesData) {
		let pauseCounter = 0;

		pauseInterval = setInterval(() => {
			pauseCounter++;

			if (pauseCounter >= CONST.PAUSE_COUNT_TICK) {
				this.endPause(enemiesData);
				clearInterval(pauseInterval);
			}

			this.setState({
				pauseTimer:
					(CONST.PAUSE_TICK_INTERVAL * CONST.PAUSE_COUNT_TICK -
					pauseCounter * CONST.PAUSE_TICK_INTERVAL) / 1000,
			});

		}, CONST.PAUSE_TICK_INTERVAL);
	};

	endPause(enemiesData) {
		this.setState({
			enemies: enemiesData,
		});

		startEnemyInterval = true;
	};

	handleEnemies() {
		if (startEnemyInterval) {
			enemyInterval = setInterval(() => {
				this.updateEnemies();
			}, CONST.ENEMY_INTERVAL);

			startEnemyInterval = false;
		}
	};

	updateEnemies() {
		const {enemies, level} = this.state;
		let newEnemies = [];
		let howManyDead = 0;

		enemies.forEach((enemy, i) => {
			let key = enemies[i].key;
			let movement = [];
			let alive = enemies[i].alive;
			let gold = enemies[i].gold;

			if (alive === false) {
				howManyDead++;
				movement = [enemies[i].top, enemies[i].left, enemies[i].location, enemies[i].borders];

			} else {
				movement = calculatePosition(enemies[i].top, enemies[i].left, enemies[i].location, enemies[i].borders);

				if (movement[4]) { //position 5
					this.decreasePlayerLife();
				}
			}

			newEnemies.push({
				key,
				index: key,
				top: movement[0],
				left: movement[1],
				location: movement[2],
				borders: movement[3],
				alive,
				gold,
				hit:0,
			})
		});

		if (howManyDead === CONST.ENEMY_COUNT_LVL[level]) {
			this.newLvl();

		} else {
			this.setState({
				enemies: newEnemies,
			});
		}
	};

	newLvl() {
		const {level} = this.state;
		let newLevel = level + 1;
		clearInterval(enemyInterval);

		if (newLevel >= CONST.ENEMY_COUNT_LVL.length) {
			this.gameOver(CONST.GAME_OVER_TEXT_WIN);
			return;
		}

		this.setState({
			level: newLevel,
		}, () => {
			this.fillDataForEnemies();
		});
	};

	removeEnemy(index) {
		let {enemies} = this.state;
		enemies[index].alive = false;

		this.setState({
			enemies,
		});
		
		this.updateGold(enemies[index].gold)
	};

	decreasePlayerLife() {
		let {playerLife} = this.state;
		playerLife--;

		this.setState({
			playerLife: playerLife > CONST.PLAYER_END_LIFE ? playerLife : CONST.PLAYER_END_LIFE,
		});
	};

	gameOver(text) {
		setTimeout(function() {
			clearInterval(enemyInterval);
			alert(text);
			window.location.reload();
		}, 20); //some low random value
	}

	shootShots() {
		const {towers} = this.state;

		towers.map((tower) => {
			let position = calculateTowerPosition(tower[0]);
			let damage = tower[1][1];
			let range = tower[2][1];
			let attackSpeed = tower[3][1]; //never been used

			this.shootShot(position[0], position[1], tower[0], damage, range);
		});
	}

	shootShot(shotLeft, shotTop, index, damage, range) {
		const {shots} = this.state;
		let target = this.findTarget(shotLeft, shotTop, range);

		if (target && target[0] > 100) {
			let height = Math.floor(Math.abs(target[1] - shotTop));
			let width = Math.floor(Math.abs(target[0] - shotLeft));
			let distance = Math.floor(Math.sqrt(height * height + width * width));
			let rotate = calculateRotation(target[0], target[1], shotLeft, shotTop, Math.floor(Math.asin(width / distance) * 180 / Math.PI));

			if (typeof rotate !== "undefined" && typeof rotate[0] !== "undefined") {
				shots.push({
					key: index,
					left: shotLeft,
					top: shotTop,
					height: distance,
					rotate: rotate[0],
				});

				this.enemyIsHit(target[2], damage);

				this.setState({
					shots,
				}, () => this.removeShot());
			}
		}
	}

	removeShot() {
		setTimeout(() => {
			this.setState({
				shots:[],
			});
		}, 100);
	}
	
	enemyIsHit(targetIndex, damage) {
		const {enemies} = this.state;
		enemies[targetIndex].hit = damage;

		this.setState({
			enemies,
		}, () => this.afterHit(targetIndex));
	}

	afterHit(targetIndex) {
		const {enemies} = this.state;
		enemies[targetIndex].hit = 0;

		this.setState({
			enemies,
		});
	};

	findTarget(shotLeft, shotTop, range) {
		const {enemies} = this.state;

		for (let i = 0; i < enemies.length; i++) {
			let target = enemies[i];

			if (target.alive &&
				target.left > shotLeft - (range - 20) &&
				target.left < shotLeft + 20 &&
				target.top > shotTop - range + 40 &&
				target.top < shotTop + range/2 - 60
			) {
				return [Math.floor(target.left + 110), Math.floor(target.top + 90), target.index];
			}
		}
		return [];
	}

	gameRunning() {
		const {playerLife, gold, level, pauseTimer, enemies, shots} = this.state;

		return (
			<React.Fragment>
				{this.handleEnemies()}
				{createEnemies(enemies, level, this.removeEnemy)}
				{createShots(shots)}
				<Life
					life={playerLife}
					type={CONST.PLAYER_TYPE}
					gameOver={this.gameOver}
				/>
				<InfoPanel
					type={CONST.PLAYER_TYPE}
					life={["Life", playerLife, CONST.SHOW_IN_INFO_PANEL_LOOP]}
					gold={["Gold", gold, CONST.SHOW_IN_INFO_PANEL_LOOP]}
					level={["Level", level + 1, CONST.SHOW_IN_INFO_PANEL_LOOP]}
					timeToWave={["Time to wave", pauseTimer, CONST.SHOW_IN_INFO_PANEL_LOOP]}
				/>
			</React.Fragment>
		)
	};
	
	updateGold(value) {
		let {gold} = this.state;

		this.setState({
			gold: gold + value,
		});
	};

	updateTowersInfo(towerAction, index, damage, range, attackSpeed) {
		let {towers} = this.state;
		const towerInfo = [index, damage, range, attackSpeed];

		switch (towerAction) {
			case CONST.TOWER_BUILD:
				towers.push(towerInfo);
				break;
			case CONST.TOWER_UPGRADE:
				for (let i = 0; i < towers.length; i++) {
					if (towers[i][0] === index) {
						towers[i] = towerInfo;
					}
				}
				break;
			case CONST.TOWER_SELL:
				towers = towers.filter(function(tower) {
					return tower[0] !== index;
				});
				break;
		}

		this.setState({
			towers,
		});
	}
	
	render() {
		const {gameStarted, gold} = this.state;
		
		return (
			<React.Fragment>
				<Map
					gameStarted={gameStarted}
					gold={gold}
					updateGold={this.updateGold}
					updateTowersInfo={this.updateTowersInfo}
				/>
				{gameStarted ? (
					this.gameRunning()
				) : (
					<Button
						text={CONST.START_BTN_TEXT}
						onClick={this.handleStartClick}
					/>
				)}
			</React.Fragment>
		);
	}
}

export default Main;


//todo
// <ul>
// <li>hide outside enemies + optimize, don't need to count them while not on the map</li>
// <li>boss take more lifes?</li>
// <li>refactor</li>
// <li>kvôli random zmene pohybu sa môže poradie nepriateľov meniť</li>
// <li>create big and slow enemies</li>
// <li>create small and fast enemies</li>
// <li>create armored enemies</li>
// <li>create some others enemies :)</li>
// <li>dmg and protection like "hip hap hop"</li>
// <li>when enemy dies, replace img with dead one and after cca 1 sec, remove it</li>
// <li></li>
// <li></li>
// </ul>