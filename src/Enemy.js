import React, { Component } from "react";
import {Foe} from "./styled";
import {CONST} from "./constants";
import {Life} from "./Life";

const BLACK_ENEMY_IMG = 'img/black_enemy.png';

export class Enemy extends Component {
	constructor() {
		super();
		this.state = {
		};
	}

	componentDidMount() {
		const {level, index} = this.props;
		
		this.setState({
			life: CONST.ENEMY_LIFE_LVL[level],
			index: index,
		})
	}

	componentDidUpdate() {
		const {hit} = this.props;

		if (hit > 0) {
			const {life} = this.state;

			this.setState({
				life: life - hit,
			}, () => this.checkIfAlive());
		}
	}
	
	checkIfAlive() {
		let {life, index} = this.state;
		
		if (life <= CONST.ENEMY_END_LIFE) {
			this.props.removeEnemy(index);
		}
	}

	render() {
		const {life, index} = this.state;
		const {top, left} = this.props;
		const foeStyle = {top: top + "px", left: left + "px"};

		return (
			<React.Fragment>
				<Foe
					style={foeStyle}
					data={index}
				>
					<Life
						type={CONST.ENEMY_TYPE}
						life={life}
					/>
					<img
						src={require('./' + BLACK_ENEMY_IMG)}
						width={CONST.ENEMY_SIZE}
						height={CONST.ENEMY_SIZE}
						alt={""}
					/>
				</Foe>
			</React.Fragment>
		);
	}
}
