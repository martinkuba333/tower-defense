import React, { Component } from "react";
import {Checkpoint} from "./styled";

export class Line extends Component {
	render() {
		const {check} = this.props;

		return (
			<React.Fragment>
				<Checkpoint
					check={check}
				/>
			</React.Fragment>
		);
	}
}
