import React, { Component } from "react";
import {Projectile} from "./styled";

const PX = "px";

export class Shots extends Component {
	render() {
		const {left, top, height, rotate} = this.props;

		let style = {
			left: left - 60 + PX,
			top: top + PX,
			height: height + PX,
			transform: "rotate(" + rotate + "deg)",
		};

		return (
			<React.Fragment>
				<Projectile
					style={style}
				/>
			</React.Fragment>
		);
	}
}
