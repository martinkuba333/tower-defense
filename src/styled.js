import styled from "styled-components";
import {CONST} from "./constants";

let center =
	`margin-left:auto;
	margin-right:auto;
	display:block;`

export const Square = styled.div`
	float:left;
	width:${CONST.SQUARE_SIZE}px;
	height:${CONST.SQUARE_SIZE}px;
	display:block;
	box-shadow:0 0 3px black;
	${props => props.isPath === true ?
		`box-shadow: inset 0 0 3px white;`
		:
		"box-shadow: inset 0 0 3px black; " +
		"cursor: pointer;" +
		`background:${props.isClicked ? "#f9ff4e" : "#efefef"};`
	}
`;

export const Board = styled.section`
	border:2px solid black;
	width: ${CONST.BOARD_WIDTH}px;
	height: ${CONST.BOARD_HEIGHT}px;
	transform: translate(${CONST.SQUARE_SIZE}px, ${CONST.SQUARE_SIZE}px);
	position:relative;
	${props => props.gameStarted ? "" : "pointer-events:none;"}
`;

export const Checkpoint = styled.div`
	width:10px;
	height:${CONST.SQUARE_SIZE * 3}px;
	background:red;
	position:absolute;
	${props => props.check === CONST.CHECK_START ?
	`top: ${CONST.SQUARE_SIZE}px; left: 0;` : 
	`bottom: ${CONST.SQUARE_SIZE}px; right: 0;`}
`;

export const Btn = styled.button`
	color:black;
	padding:5px;
	font-weight: 600;
	margin-top:3px;
	margin-bottom:3px;
	${center}
	${props => props.isClickable ? 
		"&:hover {cursor:pointer;}"
	:
		"pointer-events:none;"
	}
	
	${props => props.text === CONST.START_BTN_TEXT ? 
		`margin-top:50px;
		width: 100px;`
	: ""};
`;

export const Foe = styled.div`
	position:absolute;
	width:${CONST.ENEMY_SIZE}px;
	height:${CONST.ENEMY_SIZE}px;
	transform: translate(${CONST.SQUARE_SIZE}px, ${CONST.SQUARE_SIZE * 2}px);
	
	&:hover {
		cursor:pointer;
	}
`;

export const LifeBar = styled.div`
	position:absolute;
	${props => props.type === CONST.PLAYER_TYPE ?
		"left:" + (CONST.BOARD_WIDTH + 50) + "px;" +
		"font-size: 30px;" +
		"color: red;" +
		"font-weight: 600;"
	: ""}
	${props => props.type === CONST.ENEMY_TYPE ?
			`left:0px;
			right:0px;
			text-align:center;
			top:-12px;
			font-size: 10px;
			color: black;
			opacity:1;`
	: ""}
`;

export const PanelUl = styled.ul`
	margin-top:45px;
	position:absolute;
	
	${props => props.type === CONST.PLAYER_TYPE ? 
		`& li {
		display:inline;
		margin-right:10px;}`
		: ""}
	${props => props.type === CONST.TOWER_TYPE ?
		`width:150px;
		transform:rotate(-45deg);
		margin-top:15px;
		left: 20px;
    top: 60px;
    background: white;
    padding-left: 20px;
    border: 2px solid black;
    
		& li {
		list-style:none;}`
		: ""}
`;

export const Tower = styled.div`
	width: ${CONST.SQUARE_SIZE / 2}px;
	height: ${CONST.SQUARE_SIZE / 2}px;
	margin-left: auto;
	margin-right: auto;
	margin-top: ${CONST.SQUARE_SIZE / 4}px;
	position: relative;
	outline: 2px solid green;
	transform: rotate(45deg);
	position: relative;
	z-index: ${props => props.clicked ? 10 : 1};
	
	&:after {
		content: "";
		width: ${CONST.SQUARE_SIZE / 4}px;
		height: ${CONST.SQUARE_SIZE / 4}px;
		border-radius:50%;
		background:red;
		position: absolute;
    left: 0;
    right: 0;
    text-align: center;
    margin: auto;
    top: ${CONST.SQUARE_SIZE / 8}px;
	}
`;

export const Range = styled.div`
	width:${props => props.showRange}px;
	height:${props => props.showRange}px;
	border:1px solid red;
	border-radius:50%;
	display:block;
	position:relative;
	left:${props => props.showRange/-2 + CONST.SQUARE_SIZE/4}px;
	top:${props => props.showRange/-2 + CONST.SQUARE_SIZE/4}px;
`;

export const ModalCss = styled.div`
	width: ${props => props.width}px;
	height: ${props => props.height}px;
	border:2px solid black;
	border-top-right-radius:30%;
	border-top-left-radius:30%;
	border-bottom-right-radius:10%;
	border-bottom-left-radius:10%;
	position: relative;
	top: ${props => props.width/3}px;
	left: -${props => props.width/3}px;
	background:#efefef;
`;

export const PriceTag = styled.p`
	text-align:center;
`;

export const Projectile = styled.div`
	position:absolute;
	opacity:0.7;
	transform-origin: top left;
	width:0px;
	border:1px solid black;
	border-radius:100%;
	z-index:100;
`;
