import React, { Component } from "react";
import {LifeBar} from "./styled";
import {CONST} from "./constants";

export class Life extends Component {
	constructor() {
		super();
	}

	componentDidUpdate() {
		const {life, type, gameOver} = this.props;
		
		if (type === CONST.PLAYER_TYPE && life <= CONST.PLAYER_END_LIFE) {
			gameOver(CONST.GAME_OVER_TEXT_LOST);
		}
	}
	
	render() {
		const {life, type} = this.props;

		return (
			<React.Fragment>
				<LifeBar
					type={type}
				>
					{life}
				</LifeBar>
			</React.Fragment>
		);
	}
}
  