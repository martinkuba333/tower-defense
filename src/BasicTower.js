import React, { Component } from "react";
import {CONST} from "./constants";
import {Tower, Range} from "./styled";
import {InfoPanel} from "./InfoPanel";

export class BasicTower extends Component {
	constructor() {
		super();
		this.state = {
			towerLevel: ["TowerLevel", 1, CONST.SHOW_IN_INFO_PANEL_LOOP],
			damage: ["Damage", 3, CONST.SHOW_IN_INFO_PANEL_LOOP],
			range: ["Range", 300, CONST.SHOW_IN_INFO_PANEL_LOOP],
			attackSpeed: ["AttackSpeed", 100, CONST.SHOW_IN_INFO_PANEL_LOOP],
			sellPrice: ["Sell", null, CONST.SHOW_IN_INFO_PANEL_LOOP, CONST.SHOW_BUTTON],
			upgradePrice: ["Upgrade", null, CONST.SHOW_IN_INFO_PANEL_LOOP, CONST.SHOW_BUTTON],
		};

		this.showInfoPanel = this.showInfoPanel.bind(this);
		this.calculateSellPrice = this.calculateSellPrice.bind(this);
		this.upgradeTower = this.upgradeTower.bind(this);
		this.sellTower = this.sellTower.bind(this);
		this.afterClick = this.afterClick.bind(this);
	}

	showInfoPanel() {
		const {towerLevel, damage, attackSpeed, range, sellPrice, upgradePrice} = this.state;
		
		sellPrice[1] = this.calculateSellPrice();
		upgradePrice[1] = this.calculateUpgradePrice();

		return (
			<InfoPanel
				type={CONST.TOWER_TYPE}
				range={range}
				damage={damage}
				attackSpeed={attackSpeed}
				towerLevel={towerLevel}
				sellPrice={sellPrice}
				upgradePrice={upgradePrice}
				actionButton={this.afterClick}
			/>
		)
	}

	componentDidMount() {
		this.prepareSendTowerData(CONST.TOWER_BUILD);
	}
	
	afterClick(text) {
		const {upgradePrice, sellPrice} = this.state;

		if (text === upgradePrice[0]) {
			this.upgradeTower();
			this.prepareSendTowerData(CONST.TOWER_UPGRADE);
		}

		if (text === sellPrice[0]) {
			this.sellTower();
			this.prepareSendTowerData(CONST.TOWER_SELL);
		}
	}
	
	prepareSendTowerData(towerAction) {
		const {updateTowersInfo, squareIndex} = this.props;
		const {damage, range, attackSpeed} = this.state;

		updateTowersInfo(
			towerAction,
			squareIndex,
			damage,
			range,
			attackSpeed
		);
	}
	
	upgradeTower() {
		const {gold} = this.props;
		const {damage, range, attackSpeed} = this.state;
		let costOfUpgrade = this.calculateUpgradePrice();

		if (costOfUpgrade <= gold) {
			let {towerLevel} = this.state;
			towerLevel[1]++;
			
			range[1] += towerLevel[1] * CONST.TOWER_UPGRADE_RANGE_ADD;
			damage[1] += towerLevel[1] * CONST.TOWER_UPGRADE_DAMAGE_ADD;
			attackSpeed[1] += towerLevel[1] * CONST.TOWER_UPGRADE_ATTACK_SPEED_ADD;

			this.setState({
				towerLevel: towerLevel,
			});

			this.props.updateGold(-costOfUpgrade);
		}
	}
	
	sellTower() {
		this.props.removeTower(this.calculateSellPrice());
	}

	calculateUpgradePrice() {
		const {towerLevel} = this.state;

		return towerLevel[1] * CONST.TOWER_UPGRADE_PRICE_ADD;
	}

	calculateSellPrice() {
		const {towerLevel} = this.state;
		const {basicPrice} = this.props;
		let sumOfTower =
			basicPrice + CONST.TOWER_UPGRADE_PRICE_ADD * (towerLevel[1] - 1);

		return sumOfTower / CONST.TOWER_SELL_DISCOUNT;
	}

	render() {
		const {range} = this.state;
		const {clicked} = this.props;

		return (
			<React.Fragment>
				<Tower
					clicked={clicked}
				>
					{clicked ? (
							<React.Fragment>
								<Range showRange={range[1]} />
								{this.showInfoPanel()}
							</React.Fragment>
						) : ""
					}
				</Tower>
			</React.Fragment>
		);
	}
}
