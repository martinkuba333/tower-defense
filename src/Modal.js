import React, { Component } from "react";
import {ModalCss, PriceTag} from "./styled";
import {CONST} from "./constants";
import {Button} from "./Button";

export class Modal extends Component {
	render() {
		const {gold, price, afterClick} = this.props;

		return (
			<React.Fragment>
				<ModalCss
					width={CONST.SQUARE_SIZE * 3}
					height={CONST.SQUARE_SIZE * 3}
				>
					<PriceTag>{CONST.PRICE_TEXT}: {price} </PriceTag>
					<Button
						isClickable={gold >= price}
						text={CONST.BUY_TEXT}
						onClick={() => afterClick(CONST.BUY_TEXT)}
					/>
					<Button
						text={CONST.CLOSE_TEXT}
						onClick={() => afterClick(CONST.CLOSE_TEXT)}
					/>
				</ModalCss>
			</React.Fragment>
		);
	}
}
