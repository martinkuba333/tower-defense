import {CONST} from "./constants";
import {Enemy} from "./Enemy";
import React from "react";
import {Shots} from "./Shots";

export const calculateRotation =
	(targetLeft, targetTop, shotLeft, shotTop, rotate) => {

	if (targetLeft < shotLeft && targetTop < shotTop) {
		return [180 - rotate, "top", "left"];
	}

	if (targetLeft > shotLeft && targetTop < shotTop) {
		return [180 + rotate, "top", "right"];
	}

	if (targetLeft < shotLeft && targetTop > shotTop) {
		return [rotate, "bottom", "left"];
	}

	if (targetLeft > shotLeft && targetTop > shotTop) {
		return [360 - rotate, "bottom", "right"];
	}
};

export const getRandom = (min, max) => {
	return Math.floor(Math.random() * (max - min)) + min;
};

export const getEnemyBorders = () => {
	let result = [];

	result[CONST.ENEMY_LOCATION_FIRST] =
		getRandom(CONST.BOARD_WIDTH - CONST.SQUARE_SIZE - 40 * 3, CONST.BOARD_WIDTH - CONST.SQUARE_SIZE - CONST.ENEMY_SIZE);

	result[CONST.ENEMY_LOCATION_SECOND] =
		getRandom(CONST.BOARD_HEIGHT - CONST.SQUARE_SIZE * 8 + CONST.ENEMY_SIZE * 2 - 40 * 3, CONST.BOARD_HEIGHT - CONST.SQUARE_SIZE * 8 + CONST.ENEMY_SIZE);

	result[CONST.ENEMY_LOCATION_THIRD] =
		getRandom(CONST.SQUARE_SIZE, CONST.SQUARE_SIZE * 3 + CONST.ENEMY_SIZE);

	result[CONST.ENEMY_LOCATION_FOURTH] =
		getRandom(CONST.BOARD_HEIGHT - CONST.SQUARE_SIZE * 2 - CONST.SQUARE_SIZE * 3, CONST.BOARD_HEIGHT - CONST.SQUARE_SIZE * 2 - CONST.ENEMY_SIZE);

	result[CONST.ENEMY_LOCATION_FIFTH] = CONST.BOARD_WIDTH;

	return result;
};

export const calculateTowerPosition = (index) => {
	const boardWidth = CONST.BOARD_WIDTH / CONST.SQUARE_SIZE;

	const left = Math.floor(index % boardWidth) * CONST.SQUARE_SIZE + 123;
	const top = Math.floor(index / boardWidth) * CONST.SQUARE_SIZE + 60;

	return [left, top];
};


export const calculatePosition = (top, left, dir, borders) => {
	let newTop = top;
	let newLeft = left;
	let newDir = dir;
	let decreaseLife = false;

	switch (dir) {
		case CONST.ENEMY_LOCATION_FIRST:
			if (left < borders[CONST.ENEMY_LOCATION_FIRST]) {
				newLeft += CONST.ENEMY_SPEED;
			} else {
				newDir = CONST.ENEMY_LOCATION_SECOND;
			}
			break;
		case CONST.ENEMY_LOCATION_SECOND:
			if (top < borders[CONST.ENEMY_LOCATION_SECOND]) {
				newTop += CONST.ENEMY_SPEED;
			} else {
				newDir = CONST.ENEMY_LOCATION_THIRD;
			}
			break;
		case CONST.ENEMY_LOCATION_THIRD:
			if (left > borders[CONST.ENEMY_LOCATION_THIRD]) {
				newLeft -= CONST.ENEMY_SPEED;
			} else {
				newDir = CONST.ENEMY_LOCATION_FOURTH;
			}
			break;
		case CONST.ENEMY_LOCATION_FOURTH:
			if (top < borders[CONST.ENEMY_LOCATION_FOURTH]) {
				newTop += CONST.ENEMY_SPEED;
			} else {
				newDir = CONST.ENEMY_LOCATION_FIFTH;
			}
			break;
		case CONST.ENEMY_LOCATION_FIFTH:
			if (left < borders[CONST.ENEMY_LOCATION_FIFTH]) {
				newLeft += CONST.ENEMY_SPEED;
			} else {
				newDir = CONST.ENEMY_LOCATION_FIRST;
				newTop = getRandom(0, CONST.SQUARE_SIZE * CONST.PATH_VOLUME - CONST.ENEMY_SIZE);
				newLeft = -CONST.ENEMY_START_INDENT;
				decreaseLife = true;
			}
			break;
	}

	return [newTop, newLeft, newDir, borders, decreaseLife];
};

export const createEnemies = (enemies, level, removeEnemy) => {
	let createdEnemies = [];

	enemies.forEach((enemy, i) => {
		if (enemies[i].alive === true) {
			createdEnemies.push(
				<Enemy
					key={enemies[i].key}
					index={enemies[i].key}
					top={enemies[i].top}
					left={enemies[i].left}
					location={enemies[i].location}
					borders={enemies[i].borders}
					gold={enemies[i].gold}
					hit={enemies[i].hit}
					removeEnemy={removeEnemy}
					level={level}
				/>
			)
		}
	});

	return createdEnemies;
};

export const createShots = (shots) => {
	let newShots = [];

	for (let i = 0; i < shots.length; i++) {
		newShots.push(
			<Shots
				key={shots[i].key}
				left={shots[i].left}
				top={shots[i].top}
				height={shots[i].height}
				rotate={shots[i].rotate}
			/>
		)
	}

	return newShots;
};

// export const openMyProfile = userNick => {
// 	return () => window.open(`/${userNick}/uprava${i9}`);
// };

