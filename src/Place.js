import React, { Component } from "react";
import {Square} from "./styled";
import {CONST} from "./constants";
import {Modal} from "./Modal";
import {BasicTower} from "./BasicTower";

export class Place extends Component {
	constructor() {
		super();
		this.state = {
			isTower: false,
			towerIsBought: false,
			clickBeforeTower: true,
			clickOnTower: false,
			showBuyModal: false,
			towerPrice: CONST.TOWER_PRICE,
		};

		this.createPath = this.createPath.bind(this);
		this.showBuyModal = this.showBuyModal.bind(this);
		this.afterClick = this.afterClick.bind(this);
		this.buildTower = this.buildTower.bind(this);
		this.removeTower = this.removeTower.bind(this);
	}

	//todo think about how to not make it hardcoded
	createPath() {
		const {line, column, maxLine, maxColumn} = this.props;
		let result = false;

		if ((column !== maxColumn) &&
				(line === 1 || line === 2 || line === 3)) {
			result = true;
		}

		if ((column !== 0) &&
				(line === maxLine - 1 || line === maxLine - 2 || line === maxLine - 3)) {
			result = true;
		}

		if ((column !== 0 && column !== maxColumn) &&
				(line === 6 || line === 7 || line === 8)) {
			result = true;
		}

		if ((line === 4 || line === 5) &&
				(column === maxColumn - 1 || column === maxColumn - 2 || column === maxColumn - 3)) {
			result = true;
		}

		if ((line === 9 || line === 10) &&
				(column === 1 || column === 2 || column === 3)) {
			result = true;
		}

		this.state.isPath = result;

		return result;
	};

	switchMode() {
		let {towerIsBought, clickBeforeTower, clickOnTower} = this.state;

		if (clickBeforeTower && !towerIsBought) {
			this.setState({
				showBuyModal: true,
				clickBeforeTower: false,
			})
		} else if (!clickBeforeTower && !clickOnTower && !towerIsBought) {
			this.setState({
				showBuyModal: false,
				clickBeforeTower: true,
			})
		} else if (towerIsBought && !clickOnTower) {
			this.setState({
				clickOnTower: true,
			});
		} else if (towerIsBought && clickOnTower) {
			this.setState({
				clickOnTower: false,
			});
		}
	};

	showBuyModal() {
		const {towerPrice} = this.state;
		const {gold} = this.props;

		return(
			<Modal
				gold={gold}
				price={towerPrice}
				afterClick={this.afterClick}
			/>
		)
	};

	afterClick(result) {
		switch (result) {
			case CONST.BUY_TEXT:
				this.buildTower();
				break;
			case CONST.CLOSE_TEXT:
				break;
			default: //nothing
		}
	};

	buildTower() {
		const {isTower, towerPrice} = this.state;
		const {gold, updateGold} = this.props;

		if (!isTower) {
			//todo show range, buy option
			if (gold >= towerPrice) {
				this.setState({
					isTower: true,
					towerIsBought: true,
				});

				updateGold(-towerPrice);
			}
		}
	};
	
	removeTower(price) {
		const {updateGold} = this.props;
		updateGold(price);

		this.setState({
			isTower: false,
			towerIsBought: false,
			clickBeforeTower: true,
			clickOnTower:false,
		});
	}

	render() {
		const {isTower, towerIsBought, showBuyModal, clickOnTower, towerPrice} = this.state;
		const {updateGold, gold, updateTowersInfo, index} = this.props;
		const isPath = this.createPath();

		return (
			<React.Fragment>
				<Square
					isPath={isPath}
					isClicked={clickOnTower || showBuyModal && !towerIsBought}
					{...(!isPath && {onClick:(e) => this.switchMode(e)})}
				>
					{showBuyModal && !towerIsBought && this.showBuyModal()}
					{isTower && towerIsBought &&
						<BasicTower
							gold={gold}
							basicPrice={towerPrice}
							clicked={clickOnTower}
							updateGold={updateGold}
							removeTower={this.removeTower}
							updateTowersInfo={updateTowersInfo}
							squareIndex={index}
						/>
					}
				</Square>
			</React.Fragment>
		);
	}
}
